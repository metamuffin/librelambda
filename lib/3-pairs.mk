
pair = x: y: f: f x y
first = p: p true
second = p: p false

curry = f: x: y: f (pair x y)
uncurry = f: p: f (first p) (second p)

pboth2 = f: x:y: pair (f (first x) (first y)) (f (second x) (second y))
pboth = f: x: pair (f (first x)) (f (second x))
pflip = p: pair (second p) (first p)
