
true = x: y: x
false = x: y: y
not = flip

and = x: y: x y false
or = x: y: x true y
xor = x: y: x (not y) y
xnor = compose2 not xor
nand = compose2 not and
nor = compose2 not or
