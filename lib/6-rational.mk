
# pair of integers (p,q) where the represented value is p/q

q0 = pair i0 i1
q1 = pair i1 i1
q-1 = pair i-1 i1

int_to_rational = i: pair i i1
nat_to_rational = n: pair (nat_to_int n) i1

:ndr _qsimp_gcd = x: (nat_to_int (gcd (first (iabs (first x))) (first (iabs (second x)))))
:ndr _qsimp_divboth = x:fac: pair (idiv (first x) fac) (idiv (second x) fac)
:ndr _qsimp = x: _qsimp_divboth x (_qsimp_gcd x)

:ndr qadd = x:y: _qsimp (pair (iadd (imul (first x) (second y)) (imul (first y) (second x))) (imul (second x) (second y)))
qnegate = x: pair (inegate (first x)) (second x)
:ndr qsub = x:y: qadd x (qnegate y)
qoneoverx = pflip
:ndr qmul = x:y: _qsimp (pboth2 imul x y)
:ndr qdiv = x:y: qmul x (qoneoverx y)


q2 = pair i2 i1
q3 = pair i3 i1
q4 = pair i4 i1
q5 = pair i5 i1
q6 = pair i6 i1
q7 = pair i7 i1
q8 = pair i8 i1
q1/2 = pair i1 i2
q3/2 = pair i3 i2
q5/2 = pair i5 i2
q7/2 = pair i7 i2
q1/3 = pair i1 i3
q2/3 = pair i2 i3
q4/3 = pair i4 i3
q5/3 = pair i5 i3
q1/4 = pair i1 i4
q3/4 = pair i3 i4
q5/4 = pair i5 i4
q7/4 = pair i7 i4
q1/8 = pair i1 i8
q3/8 = pair i3 i8
q5/8 = pair i5 i8
q7/8 = pair i7 i8

q-2 = qnegate q2
q-3 = qnegate q3
q-4 = qnegate q4
q-5 = qnegate q5
q-6 = qnegate q6
q-7 = qnegate q7
q-8 = qnegate q8
q-1/2 = qnegate q1/2
q-3/2 = qnegate q3/2
q-5/2 = qnegate q5/2
q-7/2 = qnegate q7/2
q-1/3 = qnegate q1/3
q-2/3 = qnegate q2/3
q-4/3 = qnegate q4/3
q-5/3 = qnegate q5/3
q-1/4 = qnegate q1/4
q-3/4 = qnegate q3/4
q-5/4 = qnegate q5/4
q-7/4 = qnegate q7/4
q-1/8 = qnegate q1/8
q-3/8 = qnegate q3/8
q-5/8 = qnegate q5/8
q-7/8 = qnegate q7/8
