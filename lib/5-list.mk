
nil = fold:init: init
cons = x:xs: fold:init: fold x (xs fold init)
singleton = x: cons x nil

head = fallback:list: list (x:xs: x) fallback
tail = l:fold:nil: l (h:t:g: g h (t fold)) (t: nil) (h:t: t)

isempty = n: n (const false) true

fold = f:i:list: list f i
map = f:list: list (x:xs: cons (f x) xs) nil
filter = pred:list: list (x:a: (pred x) (cons x a) a) nil list

append = x: fold cons (cons x nil)
reverse = fold append nil

drop = n: n tail

index = fallback:n: compose (head fallback) (drop n)
