
# a natural number N is a function that takes f and x, then applies f to x N-times. 

succ = n: f: x: f (n f x)

0 = false
1 = succ 0

add = x: y: y succ x

_shiftinc = x: pair (second x) (succ (second x))
pred = n: first (n _shiftinc (pair false false))
sub = x: y: y pred x

mul = x: y: x (add y) false
pow = x: y: y x

iszero = n: n (const false) true
leq = x:y: iszero (sub x y)
eq = x:y: and (leq x y) (leq y x)
le = x:y: and (leq x y) (not (eq x y))
gr = compose2 not leq
geq = compose2 not le

:ndr fact = Y (self: n: (iszero n) 1 (mul n (self (pred n))))
:ndr mod = Y (self: x:n: (le x n) x (self (sub x n) n))
:ndr gcd = x:y: Y (self:n: (and (iszero (mod x n)) (iszero (mod y n))) n (self (pred n))) x
:ndr div = x:y: Y (self:n: (le n y) 0 (succ (self (sub n y)))) x

# this allows hacky way to write `add x y` as `x + y` 
+ = succ
- = pred

2 = succ 1
3 = succ 2
4 = succ 3
5 = succ 4
6 = succ 5
7 = succ 6
8 = succ 7
9 = succ 8
10 = succ 9
11 = succ 10
12 = succ 11
13 = succ 12
14 = succ 13
15 = succ 14
16 = succ 15
17 = succ 16
18 = succ 17
19 = succ 18
20 = succ 19
21 = succ 20
22 = succ 21
23 = succ 22
24 = succ 23
25 = succ 24
26 = succ 25
27 = succ 26
28 = succ 27
29 = succ 28
30 = succ 29
31 = succ 30
32 = succ 31
33 = succ 32
34 = succ 33
35 = succ 34
36 = succ 35
37 = succ 36
38 = succ 37
39 = succ 38
40 = succ 39
41 = succ 40
42 = succ 41
43 = succ 42
44 = succ 43
45 = succ 44
46 = succ 45
47 = succ 46
48 = succ 47
49 = succ 48
50 = succ 49
51 = succ 50
52 = succ 51
53 = succ 52
54 = succ 53
55 = succ 54
56 = succ 55
57 = succ 56
58 = succ 57
59 = succ 58
60 = succ 59
61 = succ 60
62 = succ 61
63 = succ 62
64 = succ 63
