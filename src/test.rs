mod parser {
    use crate::Expr::{self, *};
    pub fn parse_wrap(s: &str) -> Expr {
        Expr::parse(s, vec![], &std::collections::HashMap::new()).unwrap()
    }

    #[test]
    fn parse_id() {
        assert_eq!(parse_wrap("x: x"), Lambda(Box::new(Sym(0))));
    }
    #[test]
    fn parse_apply() {
        assert_eq!(
            parse_wrap("x: y: x y"),
            Lambda(Box::new(Lambda(Box::new(Apply(
                Box::new(Sym(1)),
                Box::new(Sym(0))
            )))))
        );
    }
    #[test]
    fn parse_parens() {
        assert_eq!(
            parse_wrap("(x: x) (x: x)"),
            Apply(
                Box::new(Lambda(Box::new(Sym(0)))),
                Box::new(Lambda(Box::new(Sym(0))))
            )
        );
    }
    #[test]
    fn parse_assoc() {
        assert_eq!(
            parse_wrap("x: y: z: x y z"),
            Lambda(Box::new(Lambda(Box::new(Lambda(Box::new(Apply(
                Box::new(Apply(Box::new(Sym(2)), Box::new(Sym(1)))),
                Box::new(Sym(0))
            )))))))
        );
    }
}

mod reduction {
    use crate::Expr::*;

    #[test]
    fn apply_id() {
        assert_eq!(
            Apply(Box::new(Lambda(Box::new(Sym(0)))), Box::new(Sym(123))).full_reduce(),
            Sym(123)
        )
    }

    #[test]
    fn apply_hole_test() {
        assert_eq!(
            Apply(Box::new(Lambda(Box::new(Sym(1)))), Box::new(Sym(0))).full_reduce(),
            Sym(0)
        )
    }

    #[test]
    fn eta_reduce() {
        assert_eq!(
            super::parser::parse_wrap("x: (y: z: z y) x").full_reduce(),
            Lambda(Box::new(Lambda(Box::new(Apply(
                Box::new(Sym(0)),
                Box::new(Sym(1))
            )))))
        )
    }

    #[test]
    fn etc() {
        assert_eq!(
            super::parser::parse_wrap("x: (y: z: z y) x").full_reduce(),
            super::parser::parse_wrap("y: z: z y")
        );
        assert_eq!(
            super::parser::parse_wrap("(x: (y: x) x) (x: y: x)").full_reduce(),
            super::parser::parse_wrap("x: y: x")
        );
    }
}
