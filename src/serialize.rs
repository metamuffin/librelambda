use crate::Expr;
use std::{collections::HashMap, fmt::Display};

impl Expr {
    pub fn serialize(&self, depth: usize) -> String {
        match self {
            Expr::Apply(x, y) => {
                format!("({} {})", x.serialize(depth), y.serialize(depth))
            }
            Expr::Lambda(e) => {
                format!("({}: {})", get_symbol(depth), e.serialize(depth + 1))
            }
            Expr::Sym(i) => {
                if *i > depth {
                    format!("ext{}", i - depth)
                } else {
                    get_symbol(depth - 1 - i)
                }
            }
        }
    }
    pub fn serialize_reusing_decls(
        &self,
        depth: usize,
        rev_decls: Option<&HashMap<Expr, String>>,
        top_simp: bool,
    ) -> String {
        if let Some(rev_decls) = rev_decls {
            if top_simp {
                if let Some(name) = rev_decls.get(self) {
                    return name.to_owned();
                }
            }
        }
        match self {
            Expr::Apply(x, y) => {
                format!(
                    "({} {})",
                    x.serialize_reusing_decls(depth, rev_decls, true),
                    y.serialize_reusing_decls(depth, rev_decls, true)
                )
            }
            Expr::Lambda(e) => {
                format!(
                    "({}: {})",
                    get_symbol(depth),
                    e.serialize_reusing_decls(depth + 1, rev_decls, true)
                )
            }
            Expr::Sym(i) => {
                if *i > depth {
                    format!("ext{}", i - depth)
                } else {
                    get_symbol(depth - 1 - i)
                }
            }
        }
    }
}

impl Display for Expr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.serialize(0)))
    }
}

fn get_symbol(index: usize) -> String {
    let k = [
        'x', 'y', 'z', 'w', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
        'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    ];
    if index < k.len() {
        format!("{}", k[index])
    } else {
        format!("ident{index}")
    }
}
