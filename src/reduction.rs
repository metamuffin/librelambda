use crate::{
    cache::{check_reduction_cache, store_reduction_cache},
    Expr::{self, *},
};

impl Expr {
    pub fn full_reduce(&self) -> Self {
        let mut e = self.to_owned();
        while let Some(new_e) = e.try_reduce() {
            e = new_e;
        }
        return e;
    }

    pub fn full_reduce_fast(&self) -> Self {
        let mut e = self.to_owned();
        while let Some(new_e) = e.try_reduce_multi() {
            e = new_e;
        }
        return e;
    }

    pub fn try_reduce_multi(&self) -> Option<Self> {
        if self.closed(0) {
            if let Some(e) = check_reduction_cache(self) {
                return e;
            }
        }
        let mut e = self.to_owned();
        let mut i = 0;
        const MAX_ITER: usize = 8;
        while let Some(new_e) = e.try_reduce_fast() {
            e = new_e;
            i += 1;
            if i >= MAX_ITER {
                break;
            }
        }
        if i == MAX_ITER {
            return self.try_reduce_fast();
        }
        let value = if i == 0 { None } else { Some(e) };
        if self.closed(0) {
            // implied that value is closed too if self is
            store_reduction_cache(self, &value);
        }
        value
    }

    pub fn try_reduce_fast(&self) -> Option<Self> {
        match self {
            Apply(box Lambda(t), k) => return Some(t.replace_sym(0, &k)),
            Lambda(box Apply(t, box Sym(0))) if !t.find(0) => return Some(t.close_sym_gap(0)),
            _ => (),
        }
        match self {
            Apply(x, y) => {
                if let Some(x) = x.try_reduce_multi() {
                    return Some(Apply(Box::new(x), y.to_owned()));
                }
                if let Some(y) = y.try_reduce_multi() {
                    return Some(Apply(x.to_owned(), Box::new(y)));
                }
            }
            Lambda(e) => {
                if let Some(e) = e.try_reduce_multi() {
                    return Some(Lambda(Box::new(e)));
                }
            }
            Sym(_) => (),
        };
        None
    }

    pub fn try_reduce(&self) -> Option<Self> {
        match self {
            Apply(box Lambda(t), k) => return Some(t.replace_sym(0, &k)),
            Lambda(box Apply(t, box Sym(0))) if !t.find(0) => return Some(t.close_sym_gap(0)),
            _ => (),
        }
        match self {
            Apply(x, y) => {
                if let Some(x) = x.try_reduce() {
                    return Some(Apply(Box::new(x), y.to_owned()));
                }
                if let Some(y) = y.try_reduce() {
                    return Some(Apply(x.to_owned(), Box::new(y)));
                }
            }
            Lambda(e) => {
                if let Some(e) = e.try_reduce() {
                    return Some(Lambda(Box::new(e)));
                }
            }
            Sym(_) => (),
        };
        None
    }

    pub fn replace_sym(&self, sym: usize, expr: &Expr) -> Self {
        match self {
            Apply(a, b) => Apply(
                Box::new(a.replace_sym(sym, expr)),
                Box::new(b.replace_sym(sym, expr)),
            ),
            Lambda(e) => Lambda(Box::new(e.replace_sym(sym + 1, expr))),
            Sym(i) => {
                if *i == sym {
                    // in case we insert deeper, add depth (=sym) to all syms in the replacement
                    expr.push_by(sym, 0)
                } else if *i > sym {
                    Sym(*i - 1)
                } else {
                    Sym(*i)
                }
            }
        }
    }

    pub fn push_by(&self, amount: usize, depth: usize) -> Self {
        match self {
            Apply(x, y) => Apply(
                Box::new(x.push_by(amount, depth)),
                Box::new(y.push_by(amount, depth)),
            ),
            Lambda(e) => Lambda(Box::new(e.push_by(amount, depth + 1))),
            Sym(s) => Sym(if *s < depth { *s } else { *s + amount }),
        }
    }

    pub fn close_sym_gap(&self, sym: usize) -> Self {
        match self {
            Apply(x, y) => Apply(
                Box::new(x.close_sym_gap(sym)),
                Box::new(y.close_sym_gap(sym)),
            ),
            Lambda(e) => Lambda(Box::new(e.close_sym_gap(sym + 1))),
            Sym(s) => Sym(if *s < sym {
                *s
            } else if *s > sym {
                *s - 1
            } else {
                unreachable!()
            }),
        }
    }

    fn find(&self, depth: usize) -> bool {
        match self {
            Apply(x, y) => x.find(depth) || y.find(depth),
            Lambda(e) => e.find(depth + 1),
            Sym(i) => *i == depth,
        }
    }

    pub fn closed(&self, depth: usize) -> bool {
        match self {
            Apply(x, y) => x.closed(depth) && y.closed(depth),
            Lambda(e) => e.closed(depth + 1),
            Sym(i) => *i < depth,
        }
    }
}
