use librelambda::env::Environment;
use std::{env::args, io::stdin, path::Path};

fn main() {
    let mut env = Environment::new();
    if args().find(|a| a == "--no-lib").is_none() {
        env.load_path(Path::new("./lib"));
    }
    println!("\x1b[2Kready");

    for line in stdin().lines() {
        let line = line.unwrap();

        match env.execute(&line, true) {
            Ok(Some(_)) => {}
            Ok(None) => {}
            Err(e) => println!("error: {}", e),
        }
    }
}
