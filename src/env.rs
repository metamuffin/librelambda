use crate::Expr;
use std::{
    collections::{HashMap, HashSet},
    fs::File,
    io::{stdout, BufRead, BufReader, Write},
    path::Path,
};

pub struct Environment {
    decls: HashMap<String, Expr>,
    rev_decls: HashMap<Expr, String>,
    config: EnvConfig,
}
#[derive(Clone)]
struct EnvConfig {
    decl_reduction: bool,
    show_reductions: bool,
    reuse_decls: bool,
    top_simp: bool,
    hide_steps: bool,
    fast: bool,
}

impl Environment {
    pub fn new() -> Self {
        Environment {
            decls: Default::default(),
            rev_decls: Default::default(),
            config: EnvConfig {
                fast: false,
                hide_steps: true,
                decl_reduction: true,
                show_reductions: false,
                reuse_decls: true,
                top_simp: true,
            },
        }
    }

    pub fn execute(&mut self, mut line: &str, interactive: bool) -> Result<Option<Expr>, String> {
        if line.starts_with("#") || line.trim().len() == 0 {
            return Ok(None);
        }

        let mut config = self.config.clone();

        while line.starts_with(":") {
            line = &line[1..];
            let (command, rest) = line.split_once(" ").unwrap_or((line, ""));
            line = rest;
            match command {
                "set" => {
                    let (var_name, value) = line.split_once("=").unwrap_or(("", ""));
                    let bool = |x: &str| x == "1" || x == "true";
                    match var_name {
                        "decl_reduction" => self.config.decl_reduction = bool(value),
                        "fast" => self.config.fast = bool(value),
                        "show_reductions" => self.config.show_reductions = bool(value),
                        "reuse_decls" => self.config.reuse_decls = bool(value),
                        "top_simp" => self.config.top_simp = bool(value),
                        "hide_steps" => self.config.hide_steps = bool(value),
                        _ => return Err(format!("unknown config parameter: {var_name}")),
                    }
                    return Ok(None);
                }

                "no_decl_reduction" | "ndr" => config.decl_reduction = false,
                _ => return Err(format!("unknown keyword: {command}")),
            }
        }

        if let Some((name, term)) = line.split_once("=") {
            let name = name.trim();
            let mut expr = Expr::parse(term, vec![], &self.decls)?;
            if config.decl_reduction {
                if config.fast {
                    expr = expr.full_reduce_fast()
                } else {
                    expr = expr.full_reduce();
                }
            }
            self.decls.insert(name.to_string(), expr.clone());
            self.rev_decls.insert(expr, name.to_string());
            return Ok(None);
        }

        let mut e = Expr::parse(line, vec![], &self.decls)?;

        if config.fast {
            e = e.full_reduce_fast()
        } else {
            let mut checked = HashSet::new();
            let show_red_limit = if config.hide_steps { 8 } else { usize::MAX };
            for i in 0.. {
                if interactive {
                    if config.show_reductions {
                        if i > show_red_limit {
                            print!("\r... {} more reduction steps ...", i - show_red_limit);
                            stdout().flush().unwrap();
                        } else {
                            if config.reuse_decls {
                                println!(
                                    "= {}",
                                    e.serialize_reusing_decls(
                                        0,
                                        Some(&self.rev_decls),
                                        config.top_simp
                                    )
                                );
                            } else {
                                println!("= {}", e)
                            }
                        }
                    } else {
                        print!("reducing... {}\r", i);
                        stdout().flush().unwrap();
                    }
                }
                checked.insert(e.to_owned());
                match e.try_reduce() {
                    Some(new_e) => {
                        if checked.contains(&new_e) {
                            break;
                        }
                        e = new_e
                    }
                    None => {
                        if interactive {
                            if i > show_red_limit && config.show_reductions {
                                println!() // finish the print!() from above
                            } else {
                                print!("\x1b[2K")
                            }
                        }
                        break;
                    }
                }
            }
        }
        if interactive {
            if config.reuse_decls {
                println!(
                    "= {}",
                    e.serialize_reusing_decls(0, Some(&self.rev_decls), config.top_simp)
                );
            } else {
                println!("= {}", e)
            }
        }
        return Ok(Some(e));
    }

    pub fn load_path(&mut self, path: &Path) {
        if path.is_dir() {
            let mut files = path
                .read_dir()
                .unwrap()
                .map(|e| e.unwrap().path().to_path_buf())
                .collect::<Vec<_>>();
            files.sort();
            for e in files {
                self.load_path(&e)
            }
        } else if path.is_file() {
            let f = File::open(path).unwrap();
            let f = BufReader::new(f);
            for (i, line) in f.lines().enumerate() {
                let line = line.unwrap();
                print!("\x1b[2K{path:?}:{i}\r");
                stdout().flush().unwrap();
                // std::thread::sleep(std::time::Duration::from_millis(5));
                if let Err(e) = self.execute(&line, false) {
                    eprintln!("error in {path:?}:{i}: {e}")
                }
            }
        }
    }
}
