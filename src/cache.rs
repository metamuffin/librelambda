use crate::Expr;
use sled::Db;
use typed_sled::Tree;

lazy_static::lazy_static! {
    static ref DB: Db = sled::open("/tmp/cache").unwrap();
    static ref CACHE: Tree<Expr, Option<Expr>> =  typed_sled::Tree::open(&DB, "reductions");
}

pub fn store_reduction_cache(start: &Expr, end: &Option<Expr>) {
    eprintln!("store {start}");
    match end {
        Some(e) => eprintln!(" -> {e}"),
        None => eprintln!(" -> doesnt have a normal form"),
    }
    CACHE.insert(start, end).unwrap();
}
pub fn check_reduction_cache(e: &Expr) -> Option<Option<Expr>> {
    eprintln!("check {e}");
    CACHE.get(e).unwrap()
}
