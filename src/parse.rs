use crate::Expr;
use std::collections::HashMap;

impl Expr {
    pub fn parse(
        mut s: &str,
        symbols: Vec<String>,
        decls: &HashMap<String, Expr>,
    ) -> Result<Self, String> {
        s = s.trim();
        if s.starts_with("(") && s.ends_with(")") {
            let mut br = 0;
            let mut ok = true;
            for c in (&s[1..s.len() - 1]).chars() {
                match c {
                    '(' => br += 1,
                    ')' => br -= 1,
                    _ => (),
                }
                if br < 0 {
                    ok = false;
                    break;
                }
            }
            if ok {
                s = &s[1..s.len() - 1]
            }
        }
        if let Some((k, b)) = s.split_once(":") {
            if k.chars()
                .find(|c| match c {
                    '(' | ')' | ' ' | '\t' | '\n' | ':' => true,
                    _ => false,
                })
                .is_none()
            {
                let mut symbols = symbols.clone();
                symbols.push(k.to_owned());
                return Ok(Self::Lambda(Box::new(Expr::parse(b, symbols, decls)?)));
            }
        }

        if let Some((f, a)) = split_once_brackets_rev(s, ' ') {
            return Ok(Self::Apply(
                Box::new(Expr::parse(f, symbols.clone(), decls)?),
                Box::new(Expr::parse(a, symbols, decls)?),
            ));
        }

        if let Some(sym) = symbols
            .iter()
            .enumerate()
            .find(|(_, v)| v.as_str() == s)
            .map(|(a, _)| a)
        {
            return Ok(Self::Sym(symbols.len() - 1 - sym));
        }
        if let Some(expr) = decls.get(s) {
            return Ok(expr.to_owned());
        }
        Err(format!("invalid expression: {:?}", s))
    }
}

#[allow(dead_code)]
fn split_once_brackets(k: &str, del: char) -> Option<(&'_ str, &'_ str)> {
    let mut br = 0;
    for (i, c) in k.chars().enumerate() {
        match c {
            '(' => br += 1,
            ')' => br -= 1,
            _ if c == del && br == 0 => return Some((&k[..i], &k[i..])),
            _ => (),
        }
    }
    None
}

fn split_once_brackets_rev(k: &str, del: char) -> Option<(&'_ str, &'_ str)> {
    let mut br = 0;
    for (i, c) in k.char_indices().rev() {
        match c {
            '(' => br -= 1,
            ')' => br += 1,
            _ if c == del && br == 0 => return Some((&k[..i], &k[i..])),
            _ => (),
        }
    }
    None
}
