#![feature(box_patterns)]

use serde::{Deserialize, Serialize};

pub mod cache;
pub mod env;
pub mod parse;
pub mod reduction;
pub mod serialize;
#[cfg(test)]
pub mod test;
#[cfg(test)]
pub mod testlib;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Expr {
    Apply(Box<Expr>, Box<Expr>),
    Lambda(Box<Expr>),
    Sym(usize),
}
