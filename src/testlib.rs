use crate::env::Environment;
use std::path::Path;

fn env() -> Environment {
    let mut e = Environment::new();
    e.load_path(Path::new("./lib"));
    e
}

macro_rules! expr_eq {
    ($e: expr, $a: expr, $b: expr) => {
        assert_eq!($e.execute($a, false), $e.execute($b, false))
    };
}

#[test]
fn basic() {
    let mut e = env();
    expr_eq!(e, "x: id x", "id");
    expr_eq!(e, "compose id", "id");
    expr_eq!(e, "compose flip flip", "id");
    expr_eq!(e, "x: const x x", "id");
}

#[test]
fn combinator() {
    let mut e = env();
    expr_eq!(e, "I I", "I");
    expr_eq!(e, "S K K", "I");
    expr_eq!(e, "S I I", "x: x x");
    expr_eq!(e, "S K I", "I");
    expr_eq!(e, "B C C", "I");
    expr_eq!(e, "B B B", "x:y:z:d: x (y z d)");
}

#[test]
fn boolean() {
    let mut e = env();
    expr_eq!(e, "not true", "false");
    expr_eq!(e, "not false", "true");
    expr_eq!(e, "compose not not", "id");
    expr_eq!(e, "and true true", "true");
    expr_eq!(e, "or true true", "true");
    expr_eq!(e, "or true false", "true");
    expr_eq!(e, "or false false", "false");
    expr_eq!(e, "nand false false", "true");
    expr_eq!(e, "xnor false false", "true");
    expr_eq!(e, "xnor false true", "false");
}
